package co.com.pineapp.examples.util;

import java.util.ArrayList;
import java.util.Collection;

import javax.portlet.PortletPreferences;
import javax.portlet.PreferencesValidator;
import javax.portlet.ValidatorException;

public class CustomPreferValidator implements PreferencesValidator {

	@Override
	public void validate(PortletPreferences pref) throws ValidatorException {
		
		
		Collection<String> keysError = new ArrayList<String>();
		
		
		try {
		for (String key : pref.getMap().keySet()) {
			if(pref.getValue(key, "").isEmpty())
				keysError.add(key);	
		}
		
		if(!keysError.isEmpty())
			throw new ValidatorException("EL valor para la preferencia es obligatorio",keysError);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
