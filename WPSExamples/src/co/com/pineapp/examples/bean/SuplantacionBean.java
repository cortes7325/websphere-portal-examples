package co.com.pineapp.examples.bean;

import java.io.Serializable;
import java.text.MessageFormat;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.ibm.portal.portlet.service.PortletServiceHome;
import com.ibm.portal.portlet.service.impersonation.ImpersonationService;



@ManagedBean(name="suplantacionBean")
@ViewScoped
public class SuplantacionBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -621686623731988859L;
	private PortletServiceHome psh;
	private String userToImpersonate;

	@PostConstruct
	public void init(){
		
        try { 
        	Context ctx = new javax.naming.InitialContext();
            psh = (PortletServiceHome) ctx.lookup(ImpersonationService.JNDI_NAME);
        } catch(javax.naming.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NamingException e) {
			e.printStackTrace();
		}
                
        
		
		
	}
	
	
	public void suplantar(){
		
		try {
			
			/*
			 * suplantar solo usuarios del REALM por defecto, si hay LDAP federado, debe cambiar la cadena con el 
			 * DN correcto
			 */
			String dn = "uid={0},o=defaultWIMFileBasedRealm";
		
			ActionRequest request = (ActionRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			ActionResponse response = (ActionResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	        ImpersonationService impersonationService = (ImpersonationService) psh.getPortletService(ImpersonationService.class);
            impersonationService.doImpersonate(request, response, MessageFormat.format(dn, userToImpersonate));
        } catch (Exception e) {
            e.printStackTrace();
        }
		
	}
	
	public String getUserToImpersonate() {
		return userToImpersonate;
	}


	public void setUserToImpersonate(String userToImpersonate) {
		this.userToImpersonate = userToImpersonate;
	}
	

}
