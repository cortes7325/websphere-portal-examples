package co.com.pineapp.examples.bean;

import java.io.Serializable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.PortletException;

import com.ibm.faces20.portlet.FacesPortlet;
import com.ibm.portal.portlet.service.PortletServiceHome;
import com.ibm.portal.portlet.service.credentialvault.CredentialVaultService;


public class CredentialVaultBean extends FacesPortlet implements Serializable{
	
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 2646737787223555881L;
	public static final String SET_PRIVATE_CV_ACTION = "SetPrivateCV";
    public static final String DELETE_PRIVATE_CV_ACTION = "DeletePrivateCV";
    public static final String CV_USERID = "CVUserID";
    public static final String CV_PASSWORD = "CVPassword";
    public static final String CV_SUBMIT = "CVSubmit";
    
    private static CredentialVaultService vaultService = null;
    
    @Override
    public void init() throws PortletException{
        super.init();
        Context context;
        try {
            context = new InitialContext();
            PortletServiceHome vaultServiceHome = (PortletServiceHome)context.lookup("portletservice/com.ibm.portal.portlet.service.credentialvault.CredentialVaultService");
            if(vaultServiceHome != null)
            	vaultService = (CredentialVaultService)vaultServiceHome.getPortletService(CredentialVaultService.class);
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }


    
    public void action(){
    	
    }
    
}

