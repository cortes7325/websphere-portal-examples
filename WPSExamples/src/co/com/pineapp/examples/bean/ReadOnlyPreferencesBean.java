package co.com.pineapp.examples.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.ReadOnlyException;
import javax.portlet.ValidatorException;




/**
 * 
 * @author LUCHO
 *
 */
@ManagedBean(name="prefBean")
public class ReadOnlyPreferencesBean {
	
	
	
	private List<String> prefs;
	
	
	private String newValue;
	
	
	@PostConstruct
	public void init(){
		
		prefs = new ArrayList<String>();
		PortletRequest request = (PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		for (String key : request.getPreferences().getMap().keySet()) {
			prefs.add(request.getPreferences().getValue(key, ""));
		}
		
		
	}
	
	
	public void edit(){
		
		PortletRequest request = (PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		try {
			
			PortletPreferences pref = request.getPreferences();
			
			pref.setValue("url", newValue);
			pref.store();
			
			
		} catch (ReadOnlyException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Preferencia de solo lectura", "Solo puede modificarla por el portlet administrador de Portlets"));
			e.printStackTrace();
		} catch (ValidatorException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Valor de preferencia no valido", "Valor de preferencia no valido"));
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		  
	}


	public List<String> getPrefs() {
		return prefs;
	}


	public void setPrefs(List<String> prefs) {
		this.prefs = prefs;
	}


	public String getNewValue() {
		return newValue;
	}


	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	
	
	

}
